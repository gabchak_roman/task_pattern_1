package com.gabchak.view;

import com.gabchak.model.pizzas.pizza.Pizza;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Map;

public class ViewImpl implements View {

    private static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_BLACK = "\u001B[30m";
    public static final String ANSI_YELLOW = "\u001B[33m";
    private static final String ANSI_RESET = "\u001B[0m";
    private static Logger log = LogManager.getLogger(ViewImpl.class);

    @Override
    public void printMessage(String message) {
        log.info(ANSI_BLACK + message + ANSI_RESET);
    }

    @Override
    public void print(Map<Pizza, Integer> pizzas) {
        if (pizzas != null) {
            printRedMessage("Pizzas: ");
            pizzas.forEach(((pizza, integer) ->
                    log.info(ANSI_YELLOW + pizza.toString() + "\n"
                            + " ".repeat(69)
                            + ANSI_BLACK + "Number of units: "
                            + ANSI_RED + integer + ANSI_RESET)));
        } else {
            log.error("No pizzas");
        }
    }

    @Override
    public void printRedMessage(String message) {
        log.info(ANSI_RED + message + ANSI_RESET);
    }
}