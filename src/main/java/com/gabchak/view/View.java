package com.gabchak.view;

import com.gabchak.model.pizzas.pizza.Pizza;

import java.util.Map;

public interface View {

    void printMessage(String message);

    void print(Map<Pizza, Integer> pizzas);

    void printRedMessage(String message);

}
