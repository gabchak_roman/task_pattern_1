package com.gabchak.controllet;

import com.gabchak.model.MenuItem;
import com.gabchak.model.menu_items.AbstractMenuItem;
import com.gabchak.model.menu_items.DummyItem;
import com.gabchak.model.menu_items.MakePizzaOrder;
import com.gabchak.view.ConsoleReader;
import com.gabchak.view.View;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static java.lang.String.format;

public class ControllerImpl implements Controller {

    private static final Logger LOG = LogManager.getLogger(ControllerImpl.class);
    private View view;
    private ConsoleReader consoleReader;
    private Map<String, MenuItem> menu;
    private MenuItem notExistingItem;

    public ControllerImpl(View view, ConsoleReader consoleReader) {
        this.view = view;
        this.consoleReader = consoleReader;
    }

    public void start() {
        buildMenu();
        startMainLoop();
    }

    private void buildMenu() {

        menu = new LinkedHashMap<>();

        List<MenuItem> items = new ArrayList<>();
        items.add(new MakePizzaOrder("1", "Pizza", view, consoleReader));
        items.add(new AbstractMenuItem("Q", "Exit", view, null) {

            @Override
            public void execute() {
                view.printMessage("Bye");
                System.exit(0);
            }
        });

        notExistingItem = new DummyItem(view, "No such menu item.");

        items.forEach(i -> menu.put(i.getKey(), i));
    }

    private void startMainLoop() {
        String keyMenu;
        do {
            outputMenu();
            view.printMessage("Please, select buildMenu point.  ");
            keyMenu = consoleReader.readLine().toUpperCase();
            cleanConsole();
            try {
                menu.getOrDefault(keyMenu, notExistingItem).execute();
            } catch (Exception e) {
                LOG.error("Can't execute menu item {}.", keyMenu, e);
            }
        } while (true);
    }

    private void outputMenu() {
        System.out.println();
        view.printMessage(" MENU:");
        menu.values().forEach(item -> view.printMessage(
                format(" %s - %s", item.getKey(), item.getTitle())));
    }

    private void cleanConsole() {
        System.out.println("\n".repeat(30));
    }
}