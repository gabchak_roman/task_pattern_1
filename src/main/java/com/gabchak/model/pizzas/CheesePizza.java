package com.gabchak.model.pizzas;

import com.gabchak.model.pizzas.pizza.Pizza;
import com.gabchak.model.pizzas.pizza.PizzaType;
import com.gabchak.model.pizzas.pizza.components.Dough;
import com.gabchak.model.pizzas.pizza.components.Sauce;
import com.gabchak.model.pizzas.pizza.components.Topping;

public class CheesePizza extends Pizza {

    public CheesePizza() {

        setName("Cheese Pizza");
        setPizzaType(PizzaType.CHEESE);
        setDough(Dough.DEEP);
        setSauce(Sauce.PLUM_TOMATO);
        addTopping(Topping.MOZZARELLA_CHEESE);
        addTopping(Topping.TOMATOES);
        addTopping(Topping.CHICKEN);
        addTopping(Topping.BLACK_OLIVE);

    }
}
