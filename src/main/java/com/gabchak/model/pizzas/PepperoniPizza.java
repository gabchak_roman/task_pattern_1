package com.gabchak.model.pizzas;

import com.gabchak.model.pizzas.pizza.Pizza;
import com.gabchak.model.pizzas.pizza.PizzaType;
import com.gabchak.model.pizzas.pizza.components.Dough;
import com.gabchak.model.pizzas.pizza.components.Sauce;
import com.gabchak.model.pizzas.pizza.components.Topping;

public class PepperoniPizza extends Pizza {

    public PepperoniPizza() {

        setName("Pepperoni Pizza");
        setPizzaType(PizzaType.PEPPERONI);
        setDough(Dough.THIN);
        setSauce(Sauce.PLUM_TOMATO);
        addTopping(Topping.CHEDDAR_CHEESE);
        addTopping(Topping.TOMATOES);
        addTopping(Topping.PEPPERONI);
        addTopping(Topping.GREEN_OLIVE);

    }
}
