package com.gabchak.model.pizzas;

import com.gabchak.model.pizzas.pizza.Pizza;
import com.gabchak.model.pizzas.pizza.PizzaType;
import com.gabchak.model.pizzas.pizza.components.Dough;
import com.gabchak.model.pizzas.pizza.components.Sauce;
import com.gabchak.model.pizzas.pizza.components.Topping;

public class VeggiePizza extends Pizza {

    public VeggiePizza() {

        setName("Veggie delicate");
        setPizzaType(PizzaType.VEGGIE);
        setDough(Dough.HAND_TOSSED);
        setSauce(Sauce.MARINARA);
        addTopping(Topping.MOZZARELLA_CHEESE);
        addTopping(Topping.TOMATOES);
        addTopping(Topping.GREEN_OLIVE);

    }
}
