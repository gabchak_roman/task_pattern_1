package com.gabchak.model.pizzas.pizza.components;

public enum Sauce {

    MARINARA, PLUM_TOMATO, PESTO

}