package com.gabchak.model.pizzas.pizza;

public enum PizzaType {

    CHEESE, VEGGIE, CLAM, PEPPERONI

}