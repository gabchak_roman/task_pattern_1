package com.gabchak.model.pizzas.pizza.components;

public enum Topping {

    PEPPERONI,
    BACON,
    BEEF,
    SAUSAGE,
    HAM,
    STEAK,
    MUSHROOM,
    TOMATOES,
    GARLIC,
    BLACK_OLIVE,
    GREEN_OLIVE,
    CHICKEN,
    ANCHOVIES,
    ONIONS,
    PINEAPPLE,
    BELL_PEPPERS,
    BASIL,
    SPINACH,
    BONELESS_WINGS,
    BUFFALO_STYLE_CHICKEN_WINGS,
    PARMESAN_CHEESE,
    MOZZARELLA_CHEESE,
    CHEDDAR_CHEESE

}
