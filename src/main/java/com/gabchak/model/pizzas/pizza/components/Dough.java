package com.gabchak.model.pizzas.pizza.components;

public enum Dough {

    THIN, STUFFED, DEEP, HAND_TOSSED

}