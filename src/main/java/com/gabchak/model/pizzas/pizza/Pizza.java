package com.gabchak.model.pizzas.pizza;

import com.gabchak.model.pizzas.pizza.components.Dough;
import com.gabchak.model.pizzas.pizza.components.Sauce;
import com.gabchak.model.pizzas.pizza.components.Topping;

import java.util.ArrayList;
import java.util.List;

public abstract class Pizza {

    private String name;
    private PizzaType pizzaType;
    private Dough dough;
    private Sauce sauce;
    private List<Topping> toppings;

    public void setPizzaType(PizzaType pizzaType) {
        this.pizzaType = pizzaType;
    }

    public void addTopping(Topping topping) {
        if (toppings == null) {
            toppings = new ArrayList<>();
        }
        toppings.add(topping);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Dough getDough() {
        return dough;
    }

    public void setDough(Dough dough) {
        this.dough = dough;
    }

    public Sauce getSauce() {
        return sauce;
    }

    public void setSauce(Sauce sauce) {
        this.sauce = sauce;
    }

    public List<Topping> getToppings() {
        return toppings;
    }

    public void setToppings(List<Topping> toppings) {
        this.toppings = toppings;
    }

    @Override
    public String toString() {
        return "Pizza{" +
                "name='" + name + '\'' +
                ", pizzaType=" + pizzaType +
                ", dough=" + dough +
                ", sauce=" + sauce +
                ", toppings=" + toppings +
                '}';
    }
}
