package com.gabchak.model.pizzas;

import com.gabchak.model.pizzas.pizza.Pizza;
import com.gabchak.model.pizzas.pizza.PizzaType;
import com.gabchak.model.pizzas.pizza.components.Dough;
import com.gabchak.model.pizzas.pizza.components.Sauce;
import com.gabchak.model.pizzas.pizza.components.Topping;

public class ClamPizza extends Pizza {

    public ClamPizza() {

        setName("Clam");
        setPizzaType(PizzaType.CLAM);
        setDough(Dough.HAND_TOSSED);
        setSauce(Sauce.PLUM_TOMATO);
        addTopping(Topping.CHEDDAR_CHEESE);
        addTopping(Topping.TOMATOES);
        addTopping(Topping.BACON);

    }
}
