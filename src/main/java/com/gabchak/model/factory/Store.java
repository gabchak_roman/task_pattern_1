package com.gabchak.model.factory;

import com.gabchak.model.factory.bakerys.Bakery;
import com.gabchak.model.factory.bakerys.DniproBakery;
import com.gabchak.model.factory.bakerys.KyivBakery;
import com.gabchak.model.factory.bakerys.LvivBakery;

public enum Store {

    LVIV(new LvivBakery()), KYIV(new KyivBakery()), DNIPRO(new DniproBakery());

    public Bakery getBakery() {
        return bakery;
    }

    Bakery bakery;

    Store(Bakery bakery) {
        this.bakery = bakery;
    }

}
