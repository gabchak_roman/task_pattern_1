package com.gabchak.model.factory.bakerys;

import com.gabchak.model.pizzas.CheesePizza;
import com.gabchak.model.pizzas.ClamPizza;
import com.gabchak.model.pizzas.PepperoniPizza;
import com.gabchak.model.pizzas.VeggiePizza;
import com.gabchak.model.pizzas.pizza.Pizza;
import com.gabchak.model.pizzas.pizza.PizzaType;

public class LvivBakery implements Bakery {

    @Override
    public Pizza createPizza(PizzaType type) {

        Pizza pizza = null;

        if (type == PizzaType.CHEESE) {
            pizza = new CheesePizza();
        }

        if (type == PizzaType.VEGGIE) {
            pizza = new VeggiePizza();
        }

        if (type == PizzaType.CLAM) {
            pizza = new ClamPizza();
        }

        if (type == PizzaType.PEPPERONI) {
            pizza = new PepperoniPizza();
        }

        return pizza;
    }
}
