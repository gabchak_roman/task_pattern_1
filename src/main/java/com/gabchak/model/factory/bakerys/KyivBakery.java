package com.gabchak.model.factory.bakerys;

import com.gabchak.model.pizzas.CheesePizza;
import com.gabchak.model.pizzas.ClamPizza;
import com.gabchak.model.pizzas.PepperoniPizza;
import com.gabchak.model.pizzas.VeggiePizza;
import com.gabchak.model.pizzas.pizza.Pizza;
import com.gabchak.model.pizzas.pizza.PizzaType;
import com.gabchak.model.pizzas.pizza.components.Dough;
import com.gabchak.model.pizzas.pizza.components.Sauce;
import com.gabchak.model.pizzas.pizza.components.Topping;

public class KyivBakery implements Bakery {

    public Pizza createPizza(PizzaType type) {

        Pizza pizza = null;

        if (type == PizzaType.CHEESE) {
            pizza = new CheesePizza();
            pizza.setSauce(Sauce.PESTO);
        }

        if (type == PizzaType.VEGGIE) {
            pizza = new VeggiePizza();
            pizza.setDough(Dough.THIN);
            pizza.setSauce(Sauce.PLUM_TOMATO);
        }

        if (type == PizzaType.CLAM) {
            pizza = new ClamPizza();
            pizza.addTopping(Topping.ANCHOVIES);
        }

        if (type == PizzaType.PEPPERONI) {
            pizza = new PepperoniPizza();
            pizza.addTopping(Topping.GARLIC);
        }

        return pizza;
    }
}
