package com.gabchak.model.factory.bakerys;

import com.gabchak.model.pizzas.CheesePizza;
import com.gabchak.model.pizzas.ClamPizza;
import com.gabchak.model.pizzas.PepperoniPizza;
import com.gabchak.model.pizzas.VeggiePizza;
import com.gabchak.model.pizzas.pizza.Pizza;
import com.gabchak.model.pizzas.pizza.PizzaType;
import com.gabchak.model.pizzas.pizza.components.Dough;
import com.gabchak.model.pizzas.pizza.components.Sauce;
import com.gabchak.model.pizzas.pizza.components.Topping;

public class DniproBakery implements Bakery {

    public Pizza createPizza(PizzaType type) {

        Pizza pizza = null;

        if (type == PizzaType.CHEESE) {
            pizza = new CheesePizza();
            pizza.setSauce(Sauce.MARINARA);
            pizza.addTopping(Topping.MOZZARELLA_CHEESE);
            pizza.addTopping(Topping.GREEN_OLIVE);
            pizza.setDough(Dough.STUFFED);
        }

        if (type == PizzaType.VEGGIE) {
            pizza = new VeggiePizza();
            pizza.addTopping(Topping.BELL_PEPPERS);
            pizza.setSauce(Sauce.PESTO);
        }

        if (type == PizzaType.CLAM) {
            pizza = new ClamPizza();
            pizza.addTopping(Topping.ONIONS);
        }

        if (type == PizzaType.PEPPERONI) {
            pizza = new PepperoniPizza();
            pizza.addTopping(Topping.SPINACH);
        }
        return pizza;
    }
}
