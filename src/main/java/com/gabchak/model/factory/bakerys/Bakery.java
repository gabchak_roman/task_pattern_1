package com.gabchak.model.factory.bakerys;

import com.gabchak.model.pizzas.pizza.Pizza;
import com.gabchak.model.pizzas.pizza.PizzaType;

public interface Bakery {

    Pizza createPizza(PizzaType type);

}