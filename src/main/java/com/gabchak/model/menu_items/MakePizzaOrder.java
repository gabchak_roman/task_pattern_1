package com.gabchak.model.menu_items;

import com.gabchak.model.PizzaList;
import com.gabchak.model.factory.Store;
import com.gabchak.model.factory.bakerys.Bakery;
import com.gabchak.model.pizzas.pizza.Pizza;
import com.gabchak.model.pizzas.pizza.PizzaType;
import com.gabchak.view.ConsoleReader;
import com.gabchak.view.View;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.lang.String.format;

public class MakePizzaOrder extends AbstractMenuItem {

    private static Logger log = LogManager.getLogger(MakePizzaOrder.class);
    private static final String NAME = "Make pizza order";
    private static final String DETAILS = "";
    private PizzaList pizzaList;

    public MakePizzaOrder(String key, String title, View view, ConsoleReader consoleReader) {
        super(key, format(TITLE_FORMAT, title, NAME, DETAILS), view, consoleReader);

    }

    @Override
    public void execute() {

        pizzaList = new PizzaList();
        orderPizza();
        view.printMessage("Your order has been successfully accepted.");
        view.print(pizzaList.getPizzaList());
        view.printMessage("Thank you for using our service.");

    }

    private void orderPizza() {
        String chose;
        do {
            Bakery bakery = getBakery();
            PizzaType type = getPizzaType();
            Pizza pizza = bakery.createPizza(type);
            setNumberOfPizzas(pizza);
            view.printMessage(" To continue ordering press any key.");
            view.printMessage(" Q - To quit.");
            chose = consoleReader.readLine();
        } while (!chose.equalsIgnoreCase("Q"));
    }

    private Bakery getBakery() {

        Map<String, Bakery> bakery = new HashMap<>();
        bakery.put("1", Store.LVIV.getBakery());
        bakery.put("2", Store.KYIV.getBakery());
        bakery.put("3", Store.DNIPRO.getBakery());

        List<String> items = new ArrayList<>();
        items.add("Please select a store location:");
        items.add("1 - Lviv");
        items.add("2 - Kyiv");
        items.add("3 - Dnipro");

        String keyMenu = getItemKey(bakery, items);

        return bakery.get(keyMenu);
    }

    private PizzaType getPizzaType() {

        Map<String, PizzaType> pizzaType = new HashMap<>();
        pizzaType.put("1", PizzaType.CHEESE);
        pizzaType.put("2", PizzaType.VEGGIE);
        pizzaType.put("3", PizzaType.CLAM);
        pizzaType.put("4", PizzaType.PEPPERONI);

        List<String> items = new ArrayList<>();
        items.add("Select pizza type:");
        items.add("1 - Cheese");
        items.add("2 - Veggie");
        items.add("3 - Clam");
        items.add("4 - Peperoni");

        String keyMenu = getItemKey(pizzaType, items);

        return pizzaType.get(keyMenu);
    }

    private void setNumberOfPizzas(Pizza pizza) {

        int numberOfPizzas;
        boolean checkNum;

        do {
            view.printMessage("Enter the number of pizzas (MAX 10):");
            numberOfPizzas = getNumber();
            consoleClean();
            checkNum = numberOfPizzas > 10 || numberOfPizzas < 1;
          if (checkNum) {
                view.printRedMessage("Max number of pizza is 10, min 1");
            }
        } while (checkNum);
        pizzaList.addPizzaToList(pizza, numberOfPizzas);

    }

    public Integer getNumber() {

        String numberOfPizzas;
        while (!NumberUtils.isDigits(
                (numberOfPizzas = consoleReader.readLine()))){
            log.warn("Wrong input.");
        }

        return Integer.parseInt(numberOfPizzas);
    }

    private String getItemKey(Map<String, ?> pizzaType, List<String> items) {

        String keyMenu;
        boolean res;

        do {
            items.forEach(item -> view.printMessage(item));
            keyMenu = consoleReader.readLine().toUpperCase();
            consoleClean();
            res = !pizzaType.containsKey(keyMenu);
            if (res) {
                log.warn("No such menu item.");
            }
        } while (res);

        return keyMenu;
    }

    private void consoleClean() {
        System.out.println("\n".repeat(50));
    }

}
