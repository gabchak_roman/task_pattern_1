package com.gabchak.model.menu_items;

import com.gabchak.model.MenuItem;
import com.gabchak.view.ConsoleReader;
import com.gabchak.view.View;

public abstract class AbstractMenuItem implements MenuItem {
    static final String TITLE_FORMAT = "%-10s - %-25s %s";
    protected View view;
    ConsoleReader consoleReader;
    private String title;
    private String key;

    protected AbstractMenuItem(
            String key, String title, View view, ConsoleReader consoleReader) {
        this.key = key;
        this.title = title;
        this.view = view;
        this.consoleReader = consoleReader;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public String getKey() {
        return key;
    }

    @Override
    public abstract void execute();
}