package com.gabchak.model.menu_items;

import com.gabchak.view.View;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DummyItem extends AbstractMenuItem {
    private static Logger LOG = LogManager.getLogger(DummyItem.class);
    private final String message;

    public DummyItem(View view, String message) {
        super(null, null, view, null);
        this.message = message;
    }

    @Override
    public void execute() {
        LOG.warn(message);
    }
}
