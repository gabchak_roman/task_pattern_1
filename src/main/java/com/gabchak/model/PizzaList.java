package com.gabchak.model;

import com.gabchak.model.pizzas.pizza.Pizza;

import java.util.LinkedHashMap;
import java.util.Map;

public class PizzaList {

    Map<Pizza, Integer> pizzaList;

    public Map<Pizza, Integer> getPizzaList() {
        return pizzaList;
    }

    public void addPizzaToList(Pizza pizza, Integer number) {
        if (pizzaList == null){
            pizzaList = new LinkedHashMap<>();
        }
        this.pizzaList.put(pizza, number);
    }

}
