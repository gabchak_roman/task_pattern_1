package com.gabchak.model;

public interface MenuItem {

    String getTitle();

    String getKey();

    void execute();

}
